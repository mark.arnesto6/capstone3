import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveShirt({shirt_id, fetchShirts, isActive}){
	const archiveShirt = (shirtId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/shirts/${shirtId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result) {
				Swal.fire({
					title: 'Shirt archived!',
					text: 'Shirt has been archived successfully',
					icon: 'success'
				})

				fetchShirts();
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'Please try again.',
					icon: 'error'
				})

				fetchShirts();
			}
		})
	}

	const activateShirt = (shirtId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/shirts/${shirtId}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'Shirt Activated!',
					text: 'Shirt has been successfully activated.',
					icon: 'success'
				})

				fetchShirts();
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'Please try again.',
					icon: 'error'
				})

				fetchShirts();
			}
		})
	}

	return(
		<>
			{isActive ?
				<Button variant='warning' size='sm' onClick={() => archiveShirt(shirt_id)}>Archive</Button>
			:
				<Button variant='success' size='sm' onClick={() => activateShirt(shirt_id)}>Activate</Button>
			}
		</>
	)
}
