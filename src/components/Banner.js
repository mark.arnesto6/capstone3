import {Button, Row, Col} from 'react-bootstrap';


export default function Banner() {
	return(
		<Row>
			<Col className="banner p-5 text-center ">
				<h1>Snowy Print Apparel</h1>
				<h2>Welcome to everyone!</h2>
			</Col>	
		</Row>
	)
}
