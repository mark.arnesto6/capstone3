import {Table, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import EditShirt from './EditShirt.js';
import ArchiveShirt from './ArchiveShirt.js';

export default function AdminView({shirtsData, fetchShirts}){
	const [shirts, setShirts] = useState([])

	useEffect(() => {
		const shirts_array = shirtsData.map(shirt => {
			return(
				<tr key={shirt._id}>
					<td>{shirt._id}</td>
					<td>{shirt.name}</td>
					<td>{shirt.description}</td>
					<td>{shirt.price}</td>
					<td>{shirt.isActive ? 'Available' : 'Unavailable'}</td>
					<td>
						<EditShirt shirt_id={shirt._id} fetchShirts={fetchShirts}/>
					</td>
					<td>
						<ArchiveShirt shirt_id={shirt._id} fetchShirts={fetchShirts} isActive={shirt.isActive}/>
					</td>
				</tr>
			)
		})

		setShirts(shirts_array);
	}, [shirtsData])

	return(
		<>
			<h1>Admin Dashboard</h1>

			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{shirts}
				</tbody>
			</Table>
		</>
	)
}
