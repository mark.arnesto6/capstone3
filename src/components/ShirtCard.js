import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import '../ShirtCard.css';

export default function ShirtCard({ shirt }) {
  const { _id, name, description, price, imageUrl } = shirt;

  return (
    <Card className="cardHighlight">
      <Card.Img
        variant="top"
        src={imageUrl}
        alt={name}
        className="shirt-card-image"
      />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{description}</Card.Text> {''}
        <Card.Text>Price: ${price}</Card.Text> {0}

        <div className="price-button">
          <Link className="btn btn-primary" to={`/shirts/${_id}`}>
            View Details
          </Link>
        </div>
      </Card.Body>
    </Card>
  );
}

ShirtCard.propTypes = {
  shirt: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
  }),
};

