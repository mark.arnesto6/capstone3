import React, { useState, useEffect } from 'react';
import ShirtCard from './ShirtCard.js';
import '../UserView.css';

export default function UserView({ shirtsData }) {
    const [shirts, setShirts] = useState([]);

    useEffect(() => {
        const activeShirts = shirtsData.map((shirt) => {
            if (shirt.isActive === true) {
                return (
                    <div className="shirt-card" key={shirt._id}>
                        <ShirtCard shirt={shirt} className="user-shirt-card" />
                    </div>
                );
            } else {
                return null;
            }
        });

        setShirts(activeShirts);
    }, [shirtsData]);

    return (
        <div className="user-view">
            <h1>Shirts</h1>
            <div className="shirt-list">
                {shirts}
            </div>
        </div>
    );
}
