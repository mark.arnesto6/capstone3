import { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom'; // Remove Navigate import if not needed
import UserContext from '../UserContext.js';

export default function Logout() {
  const { unsetUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    unsetUser();

    localStorage.removeItem('token');
    localStorage.removeItem('userId');

    // Use the navigate function to perform navigation
    navigate('/login');
  }, [unsetUser, navigate]); // Remove setUser from dependencies since it's not used

  return null;
}
