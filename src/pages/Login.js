import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){
  const {user, setUser} = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  function authenticate(event) {
        event.preventDefault();

      fetch(`${process.env.REACT_APP_API_URL}/api/users/login`,{

          method: 'POST',
          headers: {
              
              "Content-Type": "application/json"
          },
          body: JSON.stringify({

              email: email,
              password: password

          })
      })
      .then(response => response.json())
      .then(result => {
        console.log(result);

          if(result.accessToken){
            localStorage.setItem('token', result.accessToken);
            localStorage.setItem('userId', result.userId);

            retrieveUserDetails(result.accessToken, result.userId)

          setEmail('');
          setPassword('');

              Swal.fire({
                title: 'Login Success',
                text: 'You have logged in successfully!',
                icon: 'success'
              })
              
          } else {

            Swal.fire({
              title: 'Something went wrong',
              text: `${email} does not exist`,
              icon: 'warning'
            })         
          }
      })
       .catch(error => {
        console.error("Error fetching login API:", error);     
      });
    }

    const retrieveUserDetails = (token, userId) => {
      fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
        }, 
        body: JSON.stringify({
          id: userId
        })
      })
      .then(response => response.json())
      .then(result => {
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      })
    }

  useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

  return(
    (user.id !== null) ?
      <Navigate to='/shirts'/>
    :
      <Form onSubmit={(event) => authenticate(event)}>
              <h1 className="my-5 text-center">Login</h1>
              <Form.Group controlId="userEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control 
                      type="email" 
                      placeholder="Enter email"
                      value={email}
                      onChange={(event) => setEmail(event.target.value)}
                      required
                  />
              </Form.Group>

              <Form.Group controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control 
                      type="password" 
                      placeholder="Password"
                      value={password}
                      onChange={(event) => setPassword(event.target.value)}
                      required
                  />
              </Form.Group>

              <Button className="w-100 mt-2" variant="primary" type="submit" id="submitBtn" disabled={isActive == false}>
                  Submit
              </Button>
          </Form> 
  )
}
