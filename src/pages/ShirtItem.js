import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function ShirtItem() {
  const { shirtId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [shirt, setShirt] = useState({
    name: '',
    description: '',
    price: 0,
    imageUrl: '',
  });

  const order = (id) => {
    if (user.id !== null) {
      Swal.fire({
        title: 'Successfully Added!',
        text: 'You have ordered successfully!',
        icon: 'success',
      });
      navigate('/shirts');
    } else {
      Swal.fire({
        title: 'Log In or Register',
        text: 'You need to log in or register to order.',
        icon: 'warning',
      });
    }
  };

  useEffect(() => {
    // Fetch shirt details based on shirtId
    fetch(`${process.env.REACT_APP_API_URL}/api/shirts/${shirtId}`)
      .then((response) => response.json())
      .then((result) => {
        setShirt({
          name: result.name,
          description: result.description,
          price: result.price,
          imageFile: result.imageFile,
        });
      });
  }, [shirtId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col>
          <Card>
            <Card.Img variant="top" src={shirt.imageUrl} alt={shirt.name} />
            <Card.Body className="text-center">
              <Card.Title>
                <h1>{shirt.name}</h1>
              </Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{shirt.description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(shirt.price)}</Card.Text>

              {user.id !== null ? (
                <Button variant="primary" onClick={() => order(shirtId)}>
                  Order
                </Button>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Log In to Order
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
