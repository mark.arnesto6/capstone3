import { Row, Col, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import { useState, useContext, useEffect } from 'react';

export default function Profile() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [details, setDetails] = useState({});
  const [cart, setCart] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: user.id,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result._id !== 'undefined') {
          setDetails(result);
        }
      });
  }, []);

  const addToCart = (item) => {
    // Update the local cart state
    setCart([...cart, item]);

    // Update the cart on the server
    fetch(`${process.env.REACT_APP_API_URL}/api/users/updateCart`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userId: user.id,
        cart: [...cart, item], // Updated cart including the new item
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        // Handle the response if needed
      });
  };

  return (
    <>
      {user.id === null ? (
        <Navigate to="/shirts" />
      ) : (
        <Row>
          <Col>
           
            <hr />

            <h4>Contacts Information</h4>
             <h2>{`${details.firstName} ${details.lastName}`}</h2>
            <ul>
              <li>Email: {details.email}</li>
              <li>Mobile: {details.mobileNo}</li>
            </ul>

            <h4>Your Cart</h4>
            <ul>
              {cart.map((item, index) => (
                <li key={index}>{item.name} - ${item.price}</li>
              ))}
            </ul>
            
            <Button variant="primary" onClick={() => navigate('/shirts')}>
              Continue Shopping
            </Button>
          </Col>
        </Row>
      )}
    </>
  );
}
