import { Form, Button } from 'react-bootstrap';
import { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddShirt() {
    // Other states and functions ...

    const [shirtName, setShirtName] = useState(""); // Use shirtName instead of name
    const [shirtDescription, setShirtDescription] = useState(""); // Use shirtDescription instead of description
    const [shirtPrice, setShirtPrice] = useState(0); // Use shirtPrice instead of price
    const [imageFile, setImageFile] = useState(null);

    function createShirt(event) {
        event.preventDefault();

        const formData = new FormData();
        formData.append('name', shirtName);
        formData.append('description', shirtDescription);
        formData.append('price', shirtPrice);
        formData.append('imageFile', imageFile);

        let token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/api/shirts/`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
            },
            body: formData,
        })
        .then(response => response.json())
        .then(result => {
            // ...
        });
    }

    return (
        // ...
        <Form onSubmit={event => createShirt(event)}>
            {/* Other form fields ... */}
            <Form.Group>
                <Form.Label>Image:</Form.Label>
                <Form.Control type="file" onChange={event => setImageFile(event.target.files[0])} />
            </Form.Group>
            <Button variant="primary" type="submit" className="my-5">Submit</Button>
        </Form>
        // ...
    );
}

