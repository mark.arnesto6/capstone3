import ShirtCard from '../components/ShirtCard.js';
import { useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView.js';
import UserView from '../components/UserView.js';
import UserContext from '../UserContext.js';

export default function Shirts() {
  const { user } = useContext(UserContext);

  // Define your shirtsData array with image URLs
  const shirtsData = [
    {
      _id: "1",
      name: "Shirt 1",
      description: "This is the first shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt1.jpg"
    },{
      _id: "2",
      name: "Shirt 2",
      description: "This is the second shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt2.jpg"
    },{
      _id: "3",
      name: "Shirt 3",
      description: "This is the third shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt3.jpg"
    },{
      _id: "4",
      name: "Shirt 4",
      description: "This is the fourth shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt4.jpg"
    },{
      _id: "5",
      name: "Shirt 5",
      description: "This is the fifth shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt5.jpg"
    },{
      _id: "6",
      name: "Shirt 6",
      description: "This is the sixth shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt6.jpg"
    },{
      _id: "7",
      name: "Shirt 7",
      description: "This is the seventh shirt",
      price: 20,
      isActive: true,
      imageUrl: "shirt7.jpg"
    },
  ];

  return (
    <>
      {
        (user.isAdmin === true) ?
          <AdminView shirtsData={shirtsData}/> // Pass shirtsData to AdminView
        :
          <UserView shirtsData={shirtsData} /> // Pass shirtsData to UserView
      }
    </>
  );
}