import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Shirts from './pages/Shirts.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import Order from './pages/Order.js'
import ShirtItem from './pages/ShirtItem.js';
import AddShirt from './pages/AddShirt.js';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes} from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext.js';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  
  const unsetUser = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        setUser({
            id: null,
            isAdmin: null
        });
    }

 useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id: localStorage.getItem('userId')
    })
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Error fetching user details');
    }
    return response.json();
  })
  .then(result => {
    if (typeof result._id !== 'undefined') {
      setUser({
        id: result._id,
        isAdmin: result.isAdmin
      });
      if (result.isAdmin) {
        window.location.href = '/admin';
      }
    } else {
      setUser({
        id: null,
        isAdmin: null
      });
    }
  })
  .catch(error => {
    console.error('Error fetching user details:', error);
  });
}, []);


  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path='/' element={<Home/>}/>
              <Route path='/shirts' element={<Shirts/>} />
              <Route path='/shirts/:shirtId' element={<ShirtItem/>}/>
              <Route path='/shirts/add' element={<AddShirt/>}/>
              <Route path='/register' element={ <Register/>}/>
              <Route path='/login' element={<Login/>}/>
              <Route path='/logout' element={<Logout/>}/>
              <Route path='/order' element={<Order/>}/>
              <Route path='*' element={<NotFound/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

